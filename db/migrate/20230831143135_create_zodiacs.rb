class CreateZodiacs < ActiveRecord::Migration[7.0]
  def change
    create_table :zodiacs do |t|
      t.string :name
      t.string :dates

      t.timestamps
    end
  end
end
