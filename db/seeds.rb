# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Zodiac.create([
                {
                  name: 'Aries',
                  dates: 'March 21 - April 19'
                },
                {
                  name: 'Taurus',
                  dates: 'April 20 - May 20'
                },
                {
                  name: 'Gemini',
                  dates: 'May 21 - June 20'
                },
                {
                  name: 'Cancer',
                  dates: 'June 21 - July 22'
                },
                {
                  name: 'Leo',
                  dates: 'July 23 - August 22'
                },
                {
                  name: 'Virgo',
                  dates: 'August 23 - September 22'
                },
                {
                  name: 'Libra',
                  dates: 'September 23 - October 22'
                },
                {
                  name: 'Scorpio',
                  dates: 'October 23 - November 21'
                },
                {
                  name: 'Sagittarius',
                  dates: 'November 22 - December 21'
                },
                {
                  name: 'Capricorn',
                  dates: 'December 22 - January 19'
                },
                {
                  name: 'Aquarius',
                  dates: 'January 20 - February 18'
                },
                {
                  name: 'Pisces',
                  dates: 'February 19 - March 20'
                }

              ])
