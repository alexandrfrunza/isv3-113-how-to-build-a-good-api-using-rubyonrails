# ISV3-113 How to build a good API using RubyOnRails
API with list of zodiacs.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-113-how-to-build-a-good-api-using-rubyonrails/-/archive/ISV3-113-dev/isv3-113-how-to-build-a-good-api-using-rubyonrails-ISV3-113-dev.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```

create database

```bash
docker-compose exec app rake db:create
```

run migrations

```bash
docker-compose exec app rake db:migrate
```

add seeds

```bash
docker-compose exec app rails db:seed
```
## Using api


Find in unziped folder file:

```bash
zodiacs.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 