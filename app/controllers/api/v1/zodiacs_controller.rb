class Api::V1::ZodiacsController < ApplicationController
  before_action :set_zodiac, only: %i[show update destroy]

  # GET /zodiacs
  def index
    @zodiacs = Zodiac.all

    render json: @zodiacs
  end

  # GET /zodiacs/1
  def show
    render json: @zodiac
  end

  # POST /zodiacs
  def create
    @zodiac = Zodiac.new(zodiac_params)

    if @zodiac.save
      render json: @zodiac, status: :created, location: api_v1_zodiac_url(@zodiac)
    else
      render json: @zodiac.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /zodiacs/1
  def update
    if @zodiac.update(zodiac_params)
      render json: @zodiac
    else
      render json: @zodiac.errors, status: :unprocessable_entity
    end
  end

  # DELETE /zodiacs/1
  def destroy
    @zodiac.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_zodiac
    @zodiac = Zodiac.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def zodiac_params
    params.permit(:name, :dates)
  end
end
