require 'test_helper'

class ZodiacsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @zodiac = zodiacs(:one)
  end

  test 'should get index' do
    get zodiacs_url, as: :json
    assert_response :success
  end

  test 'should create zodiac' do
    assert_difference('Zodiac.count') do
      post zodiacs_url, params: { zodiac: { dates: @zodiac.dates, name: @zodiac.name } }, as: :json
    end

    assert_response :created
  end

  test 'should show zodiac' do
    get zodiac_url(@zodiac), as: :json
    assert_response :success
  end

  test 'should update zodiac' do
    patch zodiac_url(@zodiac), params: { zodiac: { dates: @zodiac.dates, name: @zodiac.name } }, as: :json
    assert_response :success
  end

  test 'should destroy zodiac' do
    assert_difference('Zodiac.count', -1) do
      delete zodiac_url(@zodiac), as: :json
    end

    assert_response :no_content
  end
end
